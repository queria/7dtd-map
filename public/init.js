function pos(posSpec) {
	[west, north] = posSpec.split(' ');

	westSuff = west[west.length - 1];
	west = parseInt(west.substr(0, west.length - 1));
	if (westSuff=='W') { west = -1 * west; }

	northSuff = north[north.length - 1];
	north = parseInt(north.substr(0, north.length - 1));
	if (northSuff=='S') { north = -1 * north; }
	return [north, west];
}

$().ready(function(){
var bounds = [[-3072,-3072], [3072, 3072]];
var cfg = {
	crs: L.CRS.Simple,
	minZoom: -3,
	maxZoom: 2,
	center: [0, 0],
	bounds: bounds,
};
var mymap = L.map('map', cfg);

var creds = ('Created by Queria using' +
	' <a href="https://www.curbolt.net/">image by Curbolt</a>' +
	' for rest see <a href="./credits.html" target="_blank">credits</a>.' +
	' Propose changes via <a href="https://gitlab.com/queria/7dtd-map">gitlab repo</a> (esp to markers.js).'
);

var baseLayers = {}

//var layerBigImage = L.imageOverlay('map/full.png', bounds, { attribution: creds});
//baseLayers['Single big image'] = layerBigImage;
// layerBigImage.addTo(mymap);

var layerTiles = L.layerGroup([
	L.tileLayer('./map/25/map_r{y}_c{x}.png', {minZoom: -3, maxZoom: -2, maxNativeZoom: -2, minNativeZoom: -2, noWrap: true, bounds:bounds, keepBuffer:2}),
	L.tileLayer('./map/50/map_r{y}_c{x}.png', {minZoom: -1, maxZoom: -1, maxNativeZoom: -1, minNativeZoom: -1, noWrap: true, bounds:bounds, keepBuffer:5}),
	L.tileLayer('./map/full/map_r{y}_c{x}.png', {minZoom: 0, maxZoom: 2, maxNativeZoom: 0, minNativeZoom: 0, noWrap: true, bounds:bounds, keepBuffer:5}),
], {attribution: creds});
baseLayers['Tiles'] = layerTiles;
layerTiles.addTo(mymap);

function mousePosFormatter(westEast) {
	var suff = ['S', 'N'];
	if(westEast) { suff = ['W', 'E']; }
	return function(param) {
		param = Math.round(param);
		if(param<0) { 
			return ((-1*param) + suff[0]);
		} else {
			return (param + suff[1]);
		}
	};
}
var lngToEast = mousePosFormatter(true);
var latToNorth = mousePosFormatter(false);
function latlngToNW(latlng) {
	if (!L.Util.isArray(latlng)) {
		latlng = [latlng.lat, latlng.lng];
	}
	return lngToEast(latlng[1]) + " " + latToNorth(latlng[0]);
}

var mousePosCfg = {
	lngFirst: true,
	lngFormatter: lngToEast,
	latFormatter: latToNorth,
	separator: ' '
};
L.control.mousePosition(mousePosCfg).addTo(mymap);

var markers = GetMapMarkers(mymap);
var layerControl = L.control.layers(baseLayers, markers, {collapsed: false, sortLayers: false, hideSingleBase: true}).addTo(mymap);
for(var lName in markers) { markers[lName].addTo(mymap); }
/*
layerControl.addOverlay(
	L.tileLayer('./map/fake/map_r{y}_c{x}.png', {minZoom: 0, maxZoom: 1, maxNativeZoom: 0, minNativeZoom: 0, noWrap: true, bounds:bounds, opacity: 0.3}), 'control-tile');
*/

$('<div/>', {class:'leaflet-control-layers-separator'}).appendTo('.leaflet-control-layers-list');
$('<div/>', {class:'leaflet-control-layers-onoff'}).text('Switch all ').appendTo('.leaflet-control-layers-list');
var allLayersOn = $('<a/>',   {class:'leaflet-control-layers-onoff-on', href: '#', text: 'ON'});
var allLayersOff = $('<a/>',   {class:'leaflet-control-layers-onoff-off', href: '#', text: 'OFF'});
allLayersOn.appendTo('.leaflet-control-layers-onoff');
$('.leaflet-control-layers-onoff').append(' / ');
allLayersOff.appendTo('.leaflet-control-layers-onoff');
var toggleAllLayers = function(evt) {
    var turnOn = evt.data;
    var layerLabels = $('.leaflet-control-layers-overlays label');
    for(var idx in layerLabels.toArray()) {
        var label = layerLabels.eq(idx);
        var isOn = $('input', label).prop('checked');
        console.log(label.text() + ' is ' + isOn + ' => ' + turnOn);
        if((turnOn && !isOn) || (!turnOn && isOn)) {
            label.click();
        }
    }
    return false;
};
allLayersOn.on('click', null, true, toggleAllLayers);
allLayersOff.on('click', null, false, toggleAllLayers);

mymap.on('click', function(ev) {
    var coords = latlngToNW(ev.latlng);
    // NOTE: should we instead use replaceState or such to not mess with history too much?
    location.hash = '#' + coords + '@' + mymap.getZoom();
	mymap.openPopup(
		coords,
		ev.latlng);
});
mymap.on('popupclose', function(ev) {
	location.hash = '';
});
(function() {
    if (location.hash != '') {
        var coords = decodeURI(location.hash);
        if(coords.length <= 1) { return; }
        coords = coords.substr(1);
        [coords, zoom] = coords.split('@');
        coords = pos(coords);
        mymap.setView(coords, zoom);
        mymap.openPopup(latlngToNW(coords), coords);
    } else {
        mymap.setView([0, 0], -2);
    }
})();

});
