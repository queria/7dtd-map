
function GetMapMarkers(map) {
    // NOTE: possibly extract marker specific css into markers.css to separate control from generic parts
    //       as also switch to div based icons so there is just the class ref here, rest of image details in css?
    var icoBook = L.icon({iconUrl: 'icons/zlatnaj/open-book.png', iconSize: [32, 32], className: 'icon iconYellow'});
    var icoCar = L.icon({iconUrl: 'icons/zlatnaj/car.png', iconSize: [32, 32], className: 'icon'});
    // var icoCave - missing
    var icoChurch = L.icon({iconUrl: 'icons/zlatnaj/monastery.png', iconSize: [32, 32], className: 'icon'});
    var icoCloths = L.icon({iconUrl: 'icons/zlatnaj/shirt.png', iconSize: [32, 32], className: 'icon'});
    var icoFarm = L.icon({iconUrl: 'icons/zlatnaj/silo.png', iconSize: [32, 32], className: 'icon'});
    var icoGas = L.icon({iconUrl: 'icons/zlatnaj/biodiesel.png', iconSize: [32, 32], className: 'icon'});
    var icoHotel = L.icon({iconUrl: 'icons/zlatnaj/motel.png', iconSize: [32, 32], className: 'icon'});
    var icoJunkyard = L.icon({iconUrl: 'icons/zlatnaj/garbage.png', iconSize: [32, 32], className: 'icon'});
    var icoMilitary = L.icon({iconUrl: 'icons/zlatnaj/soldier.png', iconSize: [32, 32], className: 'icon iconBrown'});
    var icoPills = L.icon({iconUrl: 'icons/zlatnaj/medicine.png', iconSize: [32, 32], className: 'icon iconWhite'});
    var icoShamway = L.icon({iconUrl: 'icons/zlatnaj/canned-food.png', iconSize: [32, 32], className: 'icon'});
    var icoShotgunMessiah = L.icon({iconUrl: 'icons/zlatnaj/canon.png', iconSize: [32, 32], className: 'icon'});
    var icoTrade = L.icon({iconUrl: 'icons/zlatnaj/market.png', iconSize: [32, 32], className: 'icon iconBlue'});
    var icoWorkingStiff = L.icon({iconUrl: 'icons/zlatnaj/hammer.png', iconSize: [32, 32], className: 'icon'});

    var icoOthers = L.icon({iconUrl: 'icons/zlatnaj/box.png', iconSize: [32, 32], className: 'icon'});
    //var icoOthers = L.icon({iconUrl: 'icons/zlatnaj/placeholder.png', iconSize: [32, 32], className: 'icon'});
    /* optional */
    var icoLumberMill = L.icon({iconUrl: 'icons/zlatnaj/axe.png', iconSize: [32, 32], className: 'icon'});
    //var icoHospital = L.icon({iconUrl: 'icons/zlatnaj/first-aid-kit.png', iconSize: [32, 32], className: 'icon'});
    var icoHospital = L.icon({iconUrl: 'icons/zlatnaj/hospital.png', iconSize: [32, 32], className: 'icon'});
    //var icoWestern = L.icon({iconUrl: 'icons/zlatnaj/horseshoe.png', iconSize: [32, 32], className: 'icon'});
    var icoWestern = L.icon({iconUrl: 'icons/zlatnaj/sheriff.png', iconSize: [32, 32], className: 'icon'});
    var icoMansion = L.icon({iconUrl: 'icons/zlatnaj/house.png', iconSize: [32, 32], className: 'icon'});
    var icoTower = L.icon({iconUrl: 'icons/zlatnaj/leaning-tower-of-pisa.png', iconSize: [32, 32], className: 'icon'});
    var icoSchool = L.icon({iconUrl: 'icons/zlatnaj/school-material.png', iconSize: [32, 32], className: 'icon'});
    var icoCafe = L.icon({iconUrl: 'icons/zlatnaj/tea.png', iconSize: [32, 32], className: 'icon'});
    //var icoCafe = L.icon({iconUrl: 'icons/zlatnaj/teapot.png', iconSize: [32, 32], className: 'icon'});

    function _m(coords,title, icon) {
        var opts = {title: title};
        if(icon != undefined) { opts['icon'] = icon; } else { opts['icon'] = icoOthers; }
        return L.marker(pos(coords), opts).bindPopup(title + '<br/>' + coords);
    }
    function _trader(coords) { return _m(coords, 'Trader', icoTrade); }
    function _book(coords) { return _m(coords, 'Crack a Book', icoBook); }
    function _gas(coords) { return _m(coords, 'Pass n Gas', icoGas); }
    return {
        'Cave': L.layerGroup([
            _m('996E 1019S', 'Cave'),
        ]),
        '<span class="iconYellow">Crack a Book</span>': L.layerGroup([
            _book('459W 1920N'),
            _book('2095E 5N'),
            _book('1912E 1917S'),
            _m('1762E 1661S', 'Crack a Book Tower', icoBook),
        ]),
        'Farm': L.layerGroup([
            _m('788E 902N', 'Farm', icoFarm),
            _m('231W 692N', 'Farm', icoFarm),
        ]),
        'Church': L.layerGroup([
            _m('311E 68N', 'Church', icoChurch),
            _m('5W 156S', 'Church', icoChurch),
            _m('23W 1598S', 'Church', icoChurch),
        ]),
        'Hotel/Motel': L.layerGroup([
            _m('58E 1654S', 'Hotel', icoHotel),
            _m('913E 1873S', 'Motel', icoHotel),
            _m('1879E 721S', 'Hotel', icoHotel),
        ]),
        'Car/Mechanic': L.layerGroup([
            _m('1473W 79S', 'Mechanic', icoCar),
            _m('1090W 1098S', 'Mechanic', icoCar),
            _m('2177E 351S', 'Car dealer', icoCar),
            _m('1679E 1075S', 'Car dealer', icoCar),
        ]),
        'Junkyard': L.layerGroup([
            _m('405W 887S', 'Junkyard', icoJunkyard),
            _m('1356E 713S', 'Junkyard', icoJunkyard),
        ]),
        '<span class="iconBrown">Military/Police</span>': L.layerGroup([
            _m('819W 1527N', 'Military Base', icoMilitary),
            _m('1271E 1590N', 'Military Base', icoMilitary),
            _m('532W 512N', 'Military Base', icoMilitary),
            _m('1497E 45N', 'Military Base', icoMilitary),
            _m('440E 1553S', 'Military Base', icoMilitary),
            _m('566W 1901S', 'Military Base', icoMilitary),
            _m('519W 1749N', 'Police Station', icoMilitary),
        ]),
        'Pass n Gas': L.layerGroup([
            _gas('1912W 1762N'),
            _gas('956E 1746N'),
            _gas('579E 1354N'),
            _gas('1778E 1058N'),
            _gas('1508E 642N'),
            _gas('1672W 1217N'),
            _gas('2189E 9S'),
            _gas('792E 302S'),
        ]),
        '<span class="iconWhite">Pop n Pills</span>': L.layerGroup([
            _m('1881E 1792S', 'Pop n Pills HQ', icoPills),
            _m('635E 1285N', 'Pop n Pills', icoPills),
            _m('277E 660S', 'Pop n Pills', icoPills),
            _m('1857E 1878S', 'Pop n Pills', icoPills),
        ]),
        'Savage Country': L.layerGroup([
            _m('528W 1829N', 'Savage Country', icoCloths),
            _m('1529W 981N', 'Savage Country', icoCloths),
        ]),
        'Shamway': L.layerGroup([
            _m('2112W 1092N', 'Shamway store', icoShamway),
            _m('357W 554S', 'Shamway Factory', icoShamway),
            _m('117E 449S', 'Shamway', icoShamway),
            _m('2258E 410S', 'Shamway store', icoShamway),
        ]),
        'Shotgun Messiah': L.layerGroup([
            _m('648E 1838N', 'Shotgun Messiah', icoShotgunMessiah),
            _m('878W 543S', 'Shotgun Messiah Factory', icoShotgunMessiah),
            _m('346E 1117S', 'Shotgun Messiah', icoShotgunMessiah),
        ]),
        '<span class="iconBlue">Trader</span>': L.layerGroup([
            _trader('1433E 1323N'),
            _trader('544E 363N'),
            _trader('1033E 1320S'),
            _trader('794W 1315S'),
            _trader('1187W 440N'),
        ]),
        'Western town': L.layerGroup([
            _m('445E 2178S', 'Western Town', icoWestern),
            _m('1734E 421S', 'Western Town', icoWestern),
        ]),
        'Working Stiff tools': L.layerGroup([
            _m('1457W 1881N', 'Working Stiff tools', icoWorkingStiff),
            _m('726E 653N', 'Working Stiff tools', icoWorkingStiff),
            _m('1917E 1671S', 'Working Stiff tools', icoWorkingStiff),
        ]),
        'others': L.layerGroup([
            _m('1466W 1791N', 'Street with every store - precise shop coords wanted!'),
            _m('1701W 774N', 'Cafe', icoCafe),
            _m('106E 2303S', 'Cafe', icoCafe),
            _m('1148E 613N', 'Hospital', icoHospital),
            _m('1668E 1606N', 'Lumber Mill', icoLumberMill),
            _m('577E 1415S', 'Mansion', icoMansion),
            _m('767E 1578N', 'Mansion with bunker', icoMansion),
            _m('315W 1729N', 'School', icoSchool),
            _m('217E 380N', 'Watchtower', icoTower),
            _m('1582E 1381S', 'Watchtower', icoTower),

            _m('1062W 1633S', 'Prison'),
            _m('1508E 731N', 'Electronics'),
            _m('1506E 1880S', 'Electronics'),
            _m('435W 914N', 'Pawn Shop'),
            _m('1592W 816N', 'Docks'),
            _m('1833W 111N', 'Strip club'),
            _m('1861E 29N', 'Trailer park'),
            _m('1775W 1334S', 'Oil Factory'),
            _m('1648W 868S', 'Theatre'),
            _m('410W 564S', 'Cinema'),
        ]),
	};
};

