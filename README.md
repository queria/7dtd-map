# 7DaysToDie online map / qsGameMap

Online viewable map for [7DTD A19 Navezgane](https://7daystodie.gamepedia.com/Navezgane).

**View live at https://queria.gitlab.io/7dtd-map/.**

- **Hyperlink** exact **position** in map (to be able to send others)
- Possibility to **toggle** different **point-of-interest** (POI) categories on/off (e.g. you are interested only in bookshop)
- Multiple **zoom** levels (so me/you do need to wait that long on slow connection to see details of specific location)

Over time this could/should end-up being little-bit universal game map viewer.
For that there is same basic structure but little bit more work needed too:

- **Separate map POI definition** from core part is there:
    - to be tweaked separately for each map/version (user managed)
    - possibly allow multiple to exist and select at load time?
- map **tiles** easily (re)**generated** from one big original map image, but:
    - still map bounds are hardcoded - extract together with map file
    - coords system is hardcoded - move to map info too, since now usable just for 7DtD

For reuse terms see [LICENSE](./LICENSE.md) and [credits](./public/credits.html).
